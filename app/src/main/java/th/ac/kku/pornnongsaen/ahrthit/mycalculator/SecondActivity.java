package th.ac.kku.pornnongsaen.ahrthit.mycalculator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {
    TextView tvResult;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        tvResult = (TextView) findViewById(R.id.textView);

        float re = getIntent().getExtras().getFloat("result");

        tvResult.setText("= "+ new Float(re).toString());



    }


}
