package th.ac.kku.pornnongsaen.ahrthit.mycalculator;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by Vorndervile on 23/2/2560.
 */

public class CustomView extends View{
    @TargetApi(21)
public CustomView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
    super(context, attrs, defStyleAttr, defStyleRes);
    init();
}

    public CustomView(Context context, AttributeSet attrs, int defStyleAttr){
        super(context, attrs, defStyleAttr);
        init();
    }

    public CustomView(Context context, AttributeSet attrs){
        super(context, attrs);
        init();
    }

    public CustomView(Context context){
        super(context);
        init();
    }

    private void init(){

    }

    protected void onDraw(Canvas canvas){
        super.onDraw(canvas);

        Paint p = new Paint();
        p.setColor(Color.RED);
        canvas.drawLine(0, 0, getMeasuredWidth(),getMeasuredHeight(),p);

    }
}
